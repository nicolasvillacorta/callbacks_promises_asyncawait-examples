const util = require('util');

// Este metodo del modulo util me permite convertir una funcion
//  que se maneja con callbacks, en una que se maneje con promesas.
//  es decir, ahora puedo ejecutar sleep como un async setTimeout.
// setTimeout() ejecuta cierto codigo despues de 
//  cierto intervalo de tiempo.
const sleep = util.promisify(setTimeout)


module.exports = {
    
    // Si ejecuto el codigo asi, con el throw que tiene dentro el taskOne
    //  como catchea la excepcion, al final del programa se ejecuta todo
    //  y const valueOne es undefined.
    async taskOne() {
        try{
            throw new Error('Some problem');
            await sleep(3000);
            return 'ONE VALUE';
        } catch(e){
            console.log("adentro")
            console.log(e);
        }
       
    },

    async taskTwo() {
        try{
            await sleep(2000);
            return 'TWO VALUE';
        } catch(e) {
            console.log(e);
        }
    }

}