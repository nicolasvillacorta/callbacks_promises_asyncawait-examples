const { taskOne, taskTwo } = require('./tasks');

// Lo meti dentro de esta funcion, porque si no estan los await
//  dentro de una funcion asnyc, no funciona.
 function main() {

    console.time('Measuring time');
    
    // Esta linea dice que quiero ejecutar de forma paralela
    //  todos los metodos del array.
    const results = await Promise.all([taskOne(), taskTwo()]);

    console.timeEnd('Measuring time');
    //const valueOne = await taskOne();
    //const valueTwo = await taskTwo();
    //console.log('Task One returned', valueOne);
    //console.log('Task Two returned', valueTwo);
   
    console.log('Task One returned', results[0]);
    console.log('Task Two returned', results[1]);

}

// Si el metodo no es async y no uso los await, se empiezan a 
//  ejecutar las tasks, pero el programa termina y logea undefined
//  porque todas las variables son undefined.
main();
