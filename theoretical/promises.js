// Esto son promesas. Tiene siempre el metodo 'then' cuando todo va bien,
//  el dato que esperamos y catch que atrapa todos los errores.
// A diferencia de los callbacks, es el mismo codigo pero este es
//  mucho mas ordenado, el codigo se lee mejor.
function requestHandler(req, res) {
    User.findById(req.userId)
        .then(function (user){
            return Tasks.findById(user.tasksId)
        })
        .then(function (tasks){
            tasks.completed = true;
            return tasks.save();
        })
        .then(function (){
            return('Tasks completed');
        })
        .catch(function (errors){
            res.send(errors)
        })
}