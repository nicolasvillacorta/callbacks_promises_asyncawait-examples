// Usa callbacks para manejar codigo asincrono, 
//  o al menos asi inicio.

// Funcion tipica de un servidor de node, req es la request saliendo y res la response entrante.

// User y Tasks serian DAOs. Modelos se llama en NodeJs.
// Basicamente un callback es cuando a una funcion le pasas otra funcion
//   para que haga eso con la request de la primera.

// Cuando se llama a la DB de forma asincrona, lo que hace con la
// respuesta no va abajo, va dentro de la funcion que le paso como
// parametro y de ahi en adelante se sigue ejecutando.

/*
    Todo esto es una operacion muy sencilla (forma como una piramide)
    pero es un codigo feo y dificil de leer, en este caso tiene 2 llamados
    pero una app con 10 consultas por ejemplo se torna un horror,
    ahi aparece esto conocido como piramide de la muerte o 
    callback hell, esto no es una buena solucion, no es mantenible.
    Como solucion a todo esto, surgieron las promesas.
*/

function requestHandler(req, res) {
    
    User.findById(req.usedId, function (err, user) {
        if (err) {
            res.send(err)
        } else {
            Tasks.findById(user.tasksId, function(err, tasks){
                if(err){
                    return res.send(err);
                } else {
                    tasks.completed = true;
                    tasks.save(function(err) {
                        if(err){
                            return res.send(err);
                        } else {
                            res.send('Task completed');
                        }
                    })
                }
            })
        }
    })

}