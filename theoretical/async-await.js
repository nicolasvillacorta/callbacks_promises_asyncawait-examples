// Await solo esta disponible en funciones del tipo async.
// Este codigo es incluso mas facil de leer que el de los callbacks.
// Basicamente todo lo que se hace en el lenguaje es asincrono, 
//  una consulta a la DB, una manipulacion de archivos, el consumo
//  de una API.
async function requestHandler(req, res) {

    // En este caso los errores se manejan con try catchs
    try {
        // La palabra await significa, esto va a tomar tiempo, es un 
        //  metodo asíncrono
        const user = await User.findById(req.userId);
        const tasks = await Tasks.findById(user.tasksId);
        tasks.completed = true;
        await tasks.save();
        res.send('Task completed');
    } catch (e) {
        res.send(e);
    }

}